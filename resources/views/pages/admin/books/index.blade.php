@extends('layouts.dashboard')

@section('title')
    Dentist Homepage
@endsection

@section('content')

<div class="section-content section-dashboard-home" data-aos="fade-up">
      <div class="container-fluid">
        <div class="dashboard-heading">
          <h2 class="dashboard-title">Dashboard</h2>
          <p class="dashboard-subtitle">Look what you have made today!</p>
        </div>
        <div class="dashboard-content">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <a
                    href="{{ route('books.create') }}"
                    class="btn btn-primary mb-3"
                  >
                    + Tambah Books Baru
                  </a>
                  <div class="table-responsive">
                    <table
                      class="table table-hover scroll-horizontal-vertical w-100"
                      id="crudTable"
                    >
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>NOMOR INDUK</th>
                          <th>JUDUL BUKU</th>
                          <th>PENGARANG 1</th>
                          <th>PENGARANG 2</th>
                          <th>PENGARANG 3</th>
                          <th>SINOPSIS</th>
                          <th>SUBYEK_1</th>
                          {{-- <th>SUBYEK_2</th> --}}
                          <th>BAHASA</th>
                          <th>ISBN</th>
                          <th>JUMLAH</th>
                          <th>PENERBIT</th>
                          <th>JENIS KOLEKSI</th>
                          <th>STATUS</th>
                          <th>KET STATUS KOLEKSI</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   
@endsection

@push('addon-script')
<script>
  // AJAX DataTable
  var datatable = $("#crudTable").DataTable({
    processing: true,
    serverSide: true,
    ordering: true,
    ajax: {
      url: "{!! url()->current() !!}",
    },
    columns: [
      { data: "id", name: "id" },
      { data: "nomor_induk", name: "NOMOR INDUK" },
      { data: "judul_buku", name: "JUDUL BUKU" },
      { data: "pengarang_1", name: "PENGARANG 1" },
      { data: "pengarang_2", name: "PENGARANG 2" },
      { data: "pengarang_3", name: "PENGARANG 3" },
      { data: "sinopsis", name: "SINOPSIS" },
      { data: 'subject.subyek_1', name: 'subject.subyek_1' },
      // { data: 'subjects.subyek_2', name: 'subjects.subyek_2' },
      { data: "bahasa", name: "BAHASA" },
      { data: "isbn", name: "ISBN" },
      { data: "jumlah", name: "JUMLAH" },
      { data: "penerbit", name: "PENERBIT" },
      { data: "jenis_koleksi", name: "JENIS KOLEKSI" },
      { data: "status", name: "STATUS" },
      { data: "status_koleksi", name: "KET STATUS KOLEKSI" },
      {
        data: "action",
        name: "action",
        orderable: false,
        searchable: false,
        width: "15%",
      },
    ],
  });
</script>
@endpush