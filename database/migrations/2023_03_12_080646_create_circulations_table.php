<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCirculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circulations', function (Blueprint $table) {
            $table->id();
            $table->string('nim');
            $table->string('nomor_induk');
            $table->string('jenis_koleksi');
            $table->string('tgl_pinjam');
            $table->string('tgl_kembali_1');
            $table->string('tgl_kembali_2');
            $table->string('sts_wajib');
            $table->string('sts_wajib_kbl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circulations');
    }
}
