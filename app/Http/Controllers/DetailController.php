<?php

namespace App\Http\Controllers;

use App\Models\Books;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    public function index(Request $request, $id)
    {
        $book = Books::with(['covers'])->where('id',$request->id)->firstOrFail();
        $books = Books::all();  
        return view('pages.detail', [
            'book' => $book,
            'books' => $books
        ]);
    }
}
